const express = require('express');
const passport = require('passport');
const userModel = require('../models/user');
const router = express.Router();

router.post('/register', async (req, res) => {
    const { email, password } = req.body;
    try {
        const userId = await userModel.createUser(email, password);
        res.json({ success: true, userId });
    } catch (error) {
        res.status(500).json({ success: false, message: error.message });
    }
});

router.post('/login', passport.authenticate('local', {
    successRedirect: '/',
    failureRedirect: '/login',
    failureFlash: true
}));

router.get('/logout', (req, res) => {
    req.logout();
    res.redirect('/login');
});

module.exports = router;
