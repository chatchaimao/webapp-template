**Node.js + Express + PostgreSQL Web Template**

This template will guide you in setting up a simple web application using Node.js, Express, PostgreSQL, and EJS with user authentication.

**Directory Structure**
```
/mywebsite/
|-- /node_modules/
|-- /public/
|   |-- /css/
|   |   |-- style.css
|-- /routes/
|   |-- index.js
|   |-- users.js
|-- /views/
|   |-- /partials/
|   |   |-- header.ejs
|   |   |-- footer.ejs
|   |   |-- menu.ejs
|   |-- index.ejs
|-- /models/
|   |-- user.js
|-- .gitignore
|-- app.js
|-- package.json
```
**Initialization & Dependencies**

First, initialize a new Node.js project and install the necessary dependencies:
```
mkdir mywebsite
cd mywebsite
npm init -y
npm install express ejs pg passport passport-local express-session bcryptjs
```
**Setting Up app.js**

Copy and paste the following code into your app.js:
```
const express = require('express');
const session = require('express-session');
const passport = require('passport');

const app = express();

const { Pool } = require('pg');
const pool = new Pool({
  user: 'username',
  host: 'localhost',
  database: 'mydatabase',
  password: 'password',
  port: 5432,
});

app.use(express.static('public'));
app.use(express.urlencoded({ extended: true }));
app.use(session({
  secret: 'my_secret_key',
  resave: false,
  saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());

app.set('view engine', 'ejs');

app.use('/', require('./routes/index'));
app.use('/users', require('./routes/users'));

app.listen(3000, () => {
  console.log('Server is running on port 3000');
});
```
**Routes**

For routes/index.js:
```
const express = require('express');
const router = express.Router();

router.get('/', (req, res) => {
  res.render('index', { user: req.user });
});

module.exports = router;
```
For routes/users.js, handle registration, login, and logout using passport-local.

**EJS Templates**

views/partials/header.ejs:
```
<!DOCTYPE html>
<html>
<head>
    <title>My Website</title>
    <link rel="stylesheet" href="/css/style.css">
</head>
<body>
```
views/partials/footer.ejs:
```
</body>
</html>
```
views/partials/menu.ejs:
```
<div class="menu">
    <!-- Menu links go here -->
</div>
```
views/index.ejs:
```
<%- include('./partials/header') %>
<div class="container">
    <%- include('./partials/menu') %>
    <div class="content">
        <!-- Main content goes here -->
    </div>
</div>
<%- include('./partials/footer') %>
```
**Styling**

For public/css/style.css:
```
.container {
    display: flex;
}

.menu {
    flex: 1;
}

.content {
    flex: 3;
}
```
**User Model & Local Authentication**

In models/user.js, set up the logic for the User and interact with the PostgreSQL database. Use passport-local in your routes to handle user registration, login, and authentication logic.

**models/user.js:**

First, you'll want to set up a User model that interacts with the PostgreSQL database using the pg library.
```
const { Pool } = require('pg');
const bcrypt = require('bcryptjs');

const pool = new Pool({
    user: 'username',
    host: 'localhost',
    database: 'mydatabase',
    password: 'password',
    port: 5432
});

const getUserByEmail = async (email) => {
    const result = await pool.query('SELECT * FROM users WHERE email = $1', [email]);
    return result.rows[0];
};

const createUser = async (email, password) => {
    const hashedPassword = await bcrypt.hash(password, 10);
    const result = await pool.query('INSERT INTO users (email, password) VALUES ($1, $2) RETURNING id', [email, hashedPassword]);
    return result.rows[0].id;
};

const comparePassword = (inputPassword, hashedPassword) => {
    return bcrypt.compare(inputPassword, hashedPassword);
};

module.exports = {
    getUserByEmail,
    createUser,
    comparePassword
};
```
Make sure your PostgreSQL has a table named users with columns id, email, and password.

**Passport Setup:**

First, set up passport in your main file (app.js):
```
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const userModel = require('./models/user');

passport.use(new LocalStrategy(
    async (username, password, done) => {
        const user = await userModel.getUserByEmail(username);
        if (!user) return done(null, false, { message: 'Incorrect username.' });

        const isMatch = await userModel.comparePassword(password, user.password);
        if (!isMatch) return done(null, false, { message: 'Incorrect password.' });

        return done(null, user);
    }
));

passport.serializeUser((user, done) => {
    done(null, user.id);
});

passport.deserializeUser(async (id, done) => {
    const user = await userModel.getUserById(id);
    done(null, user);
});
```
**Registration and Authentication in routes/users.js:**

```
const express = require('express');
const passport = require('passport');
const userModel = require('../models/user');
const router = express.Router();

router.post('/register', async (req, res) => {
    const { email, password } = req.body;
    try {
        const userId = await userModel.createUser(email, password);
        res.json({ success: true, userId });
    } catch (error) {
        res.status(500).json({ success: false, message: error.message });
    }
});

router.post('/login', passport.authenticate('local', {
    successRedirect: '/',
    failureRedirect: '/login',
    failureFlash: true
}));

router.get('/logout', (req, res) => {
    req.logout();
    res.redirect('/login');
});

module.exports = router;
```
Ensure to handle registration in your frontend by having a form that posts to /register with email and password fields, and similarly a login form that posts to /login.

This is a basic setup. You'd need to expand on error handling, sessions, and other necessary functionalities for a full-fledged application.




